/**
 * Symbol and its matching decimal value
 */
const converter = {
  num: [1, 4, 5, 9, 10, 40, 50, 90, 100, 400, 500, 900, 1000],
  sym: ["I", "IV", "V", "IX", "X", "XL", "L", "XC", "C", "CD", "D", "CM", "M"]
};

/**
 * Take a positive integer as its parameter and returning a string containing
 * the Roman Numeral representation of that integer.
 */
const romanNumeral = (num: number): string => {
  // without the line over the top symbol, the limitation is 3999 = MMMCMXCIX
  if (num < 1 || num > 3999) {
    return '';
  }

  let roman = '';

  let i = 12;
  // start from larger number to smallest
  while (i >= 0) {
    if (converter.num[i] <= num) {
      // convert with matching symbol
      roman += converter.sym[i];
      // and substract from current
      num -= converter.num[i];
    } else {
      --i;
    }
  }

  return roman;
}

export default romanNumeral;
