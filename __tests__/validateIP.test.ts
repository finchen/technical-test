import validateIP, { validateIP2 } from "../src/validateIP";

describe("validateIP", () => {

  it("should be valid IPv4", () => {
    expect(validateIP('1.2.3.4')).toBeTruthy();
    expect(validateIP('123.45.67.89')).toBeTruthy();
    expect(validateIP('127.0.0.1')).toBeTruthy();
    expect(validateIP('0.0.0.0')).toBeTruthy();

    expect(validateIP2('1.2.3.4')).toBeTruthy();
    expect(validateIP2('123.45.67.89')).toBeTruthy();
    expect(validateIP2('127.0.0.1')).toBeTruthy();
    expect(validateIP2('0.0.0.0')).toBeTruthy();
  });

  it("should not validate IP with a byte > 255", () => {
    expect(validateIP('123.456.78.90')).toBeFalsy();
    expect(validateIP2('123.456.78.90')).toBeFalsy();
  });

  it("should not validate IP without 4 bytes", () => {
    expect(validateIP('1.2.3')).toBeFalsy();
    expect(validateIP('1.2.3.4.5')).toBeFalsy();

    expect(validateIP2('1.2.3')).toBeFalsy();
    expect(validateIP2('1.2.3.4.5')).toBeFalsy();
  });

  it("should not be valid with leading 0", () => {
    expect(validateIP('123.045.067.089')).toBeFalsy();
    expect(validateIP2('123.045.067.089')).toBeFalsy();
    expect(validateIP('127.01.0.1')).toBeFalsy();
    expect(validateIP2('127.01.0.1')).toBeFalsy();
  });

  it("should not be valid if part is not a byte", () => {
    expect(validateIP('x.x.x.x')).toBeFalsy();
    expect(validateIP2('x.x.x.x')).toBeFalsy();
  });

});
