import add from "./add";
import encode from "./encode";
import findEvenIndex from "./findEvenIndex";
import hamming from "./hamming";
import romanNumeral from "./romanNumeral";
import validateIP from "./validateIP";

module.exports.add = add;
module.exports.findEvenIndex = findEvenIndex;
module.exports.encode = encode;
module.exports.hamming = hamming;
module.exports.romanNumeral = romanNumeral;
module.exports.validateIP = validateIP;

