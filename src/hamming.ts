/**
 * H  =  2i × 3j × 5k
 * where  i, j, k  ≥  0
 *
 * @returns nth hamming number
 */
const hamming = (nth: number): number => {

  if (nth <= 0) {
    throw new Error("nth must be greater than  0")
  }

  // The sequence of Hamming numbers begins with the number 1
  const h = [1];

  // Track multiples
  let x2 = 2;
  let x3 = 3;
  let x5 = 5;

  // Track last power used sequence
  let lastI = 0;
  let lastJ = 0;
  let lastK = 0;

  for (let i = 1; i < nth; i++) {
    // The next hamming number is the lowest of the next multiple of 2, 3, and 5
    const m = Math.min(x2, x3, x5);
    h.push(m);

    // Increment power sequence of the multiplier and calculate the next multiple for the power used
    if (x2 === m) {
      lastI++;
      x2 = 2 * h[lastI];
    }
    if (x3 === m) {
      lastJ++;
      x3 = 3 * h[lastJ];
    }
    if (x5 === m) {
      lastK++;
      x5 = 5 * h[lastK];
    }
  }
  return h[nth - 1];
}

export default hamming;
