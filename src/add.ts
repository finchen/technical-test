/**
 * Takes two binary numbers as strings and returns their sum as a string.
 * Simple way
 */
const add = (binary1: string, binary2: string): string => {
  return (parseInt(binary1, 2) + parseInt(binary2, 2)).toString(2);
}

/**
 * Remove unnecessary leading 0 using capture group either:
 */
export const removeLeadingZeros = (s: string): string => {
  // 0+(?=1): Match 1 or more zeroes that must be followed by 1
  // | or
  // 0+(?=0$): Match 1 or more zeroes that must be followed by one 0 and end
  return s.replace(/^(?:0+(?=1)|0+(?=0$))/, '');
}

/**
 * Takes two binary numbers as strings and returns their sum as a string.
 * Proper binary way: sum each binary digit individually and carry over when it needs too
 */
export const addAnt = (binary1: string, binary2: string): string => {

  const longest = binary1.length >= binary2.length ? binary1 : binary2;
  const shortest = binary1.length < binary2.length ? binary1 : binary2;

  const longestDigits = longest.split('').reverse();
  const shortestDigits = shortest.split('').reverse();

  /**
   * The binary string result reversed
   */
  const result: Array<string> = [];
  /**
   * for each iteration, When 1+1 result is 0 carry over is 1
   * if carry over is already 1 result is 1 carry over is 1
   */
  let carryOver = 0;

  // Loop over longest binary and do simply binary addition
  for (let i = 0; i < longestDigits.length; i++) {
    const digit1 = longestDigits[i];
    const digit2 = i in shortestDigits ? shortestDigits[i] : "0";
    let sum: number = carryOver + parseInt(digit1) + parseInt(digit2);

    if (sum >= 2) {
      sum = sum % 2;
      carryOver = 1;
    } else {
      carryOver = 0;
    }
    result.push(sum.toString());
  }

  if (carryOver > 0) {
    result.push(carryOver.toString());
  }

  return removeLeadingZeros(result.reverse().join(''));
}

export default add;
