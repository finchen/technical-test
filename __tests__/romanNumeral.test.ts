import romanNumeral from "../src/romanNumeral";

describe("romanNumeral", () => {

  it("should convert 1", () => {
    expect(romanNumeral(1)).toBe('I');
  });
  it("should convert 5", () => {
    expect(romanNumeral(5)).toBe('V');
  });
  it("should convert 10", () => {
    expect(romanNumeral(10)).toBe('X');
  });
  it("should convert 50", () => {
    expect(romanNumeral(50)).toBe('L');
  });
  it("should convert 100", () => {
    expect(romanNumeral(100)).toBe('C');
  });
  it("should convert 500", () => {
    expect(romanNumeral(500)).toBe('D');
  });
  it("should convert 1000", () => {
    expect(romanNumeral(1000)).toBe('M');
  });
  it("should convert 3", () => {
    expect(romanNumeral(3)).toBe('III');
  });
  it("should convert 1990", () => {
    expect(romanNumeral(1990)).toBe('MCMXC');
  });
  it("should convert 2008", () => {
    expect(romanNumeral(2008)).toBe('MMVIII');
  });
  it("should convert 1666", () => {
    expect(romanNumeral(1666)).toBe('MDCLXVI');
  });
  it("should convert 3999", () => {
    expect(romanNumeral(3999)).toBe('MMMCMXCIX');
  });
  it("should convert 1717", () => {
    expect(romanNumeral(1717)).toBe('MDCCXVII');
  });
  it("should convert 1509", () => {
    expect(romanNumeral(1509)).toBe('MDIX');
  });
  it("should not convert too large number", () => {
    expect(romanNumeral(4999)).toBe('');
  });
});
