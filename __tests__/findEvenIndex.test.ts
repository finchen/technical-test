import findEvenIndex from "../src/findEvenIndex";

describe("Equal Sides of an Array", () => {

  it("should find a even index", () => {
    expect(findEvenIndex([1, 2, 3, 4, 3, 2, 1])).toBe(3);
    expect(findEvenIndex([1, 100, 50, -51, 1, 1])).toBe(1);
    expect(findEvenIndex([20, 10, 30, 10, 10, 15, 35])).toBe(3);
    expect(findEvenIndex([4, 5, 6, 7, 8, 9, 10, 9, 8, 7, 6, 5, 4])).toBe(6);
  });

  it("should return the same index even when value at the even position changes", () => {
    expect(findEvenIndex([10, 10, 1, 20])).toBe(2);
    expect(findEvenIndex([10, 10, 2, 20])).toBe(2);
    expect(findEvenIndex([10, 10, -1, 20])).toBe(2);
  });

  it("should not find any even index", () => {
    expect(findEvenIndex([1, 2, 3, 4, 5, 6])).toBe(-1);
    expect(findEvenIndex([-3560, -12000, 2000, -9000])).toBe(-1);
  });

  it("with array of single element should return 0", () => {
    expect(findEvenIndex([1])).toBe(0);
    expect(findEvenIndex([999])).toBe(0);
  });

  it("with array element sum of 0 excluding the first element. should return 0", () => {
    expect(findEvenIndex([20, 10, -80, 10, 10, 15, 35])).toBe(0);
  });

  it("with array element sum of 0 excluding last element. should return arr.length - 1", () => {
    const arr = [10, -80, 10, 10, 15, 35, 20];
    expect(findEvenIndex(arr)).toBe(arr.length - 1);
  });

  it("should return -1 when array is empty", () => {
    expect(findEvenIndex([])).toBe(-1);
  });

  it("should return -1 when array is greater or equal 1000", () => {
    const largeArray = [...Array(1000).keys()];
    expect(findEvenIndex(largeArray)).toBe(-1);
  });

  it("should return 0 when array is empty but inialized with a fixed length  (Empty arrays are equal to 0 in this problem)", () => {
    const emptyArray = new Array(60);
    expect(findEvenIndex(emptyArray)).toBe(0);
  });

  it("should return -1 when array is greater or equal 1000 (even when empty)", () => {
    const largeArray = new Array(1500);
    expect(findEvenIndex(largeArray)).toBe(-1);
  });
});
