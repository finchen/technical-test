/**
 * The goal of this exercise is to convert a string to a new string where each character
 * in the new string is "(" if that character appears only once in the original string, or ")"
 * if that character appears more than once in the original string.
 * Ignore capitalization when determining if a character is a duplicate.
 */
const encode = (original: string): string => {
  const characters = original.toLocaleLowerCase().split('');
  let result = "";
  characters.forEach(char => {
    const occurences = characters.reduce((prev, curr) => curr === char ? prev + 1 : prev, 0);
    result += occurences > 1 ? ")" : "(";
  });

  return result;
}

export default encode;
