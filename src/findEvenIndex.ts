
/**
 * You are going to be given an array of integers.
 * Your job is to take that array and find an index N where the sum of the integers to the left of N is equal to the sum of the integers to the right of N.
 * If there is no index that would make this happen, return -1.
 *
 * @param arr An integer array of length 0 < arr < 1000. The numbers in the array can be any integer positive or negative.
 * @returns The lowest index N where the side to the left of N is equal to the side to the right of N. If you do not find an index that fits these rules, then you will return -1.
 */
const findEvenIndex = (arr: Array<number>): number => {
  if (arr.length > 999) {
    return -1;
  }
  for (let i = 0; i < arr.length; i++) {
    const sumLeft = arr.slice(0, i).reduce((a, b) => a + b, 0);
    const sumRight = arr.slice(i + 1).reduce((a, b) => a + b, 0);

    if (sumLeft === sumRight) {
      return i;
    }
  }
  return -1;
}

export default findEvenIndex;
