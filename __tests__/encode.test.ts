import encode from "../src/encode";

describe("encode", () => {

  it("should encode din", () => {
    expect(encode("din")).toBe("(((");
  });
  it("should encode recede", () => {
    expect(encode("recede")).toBe("()()()");
  });
  it("should ignore capitalization", () => {
    expect(encode("Success")).toBe(")())())");
  });
  it("should encode (( @", () => {
    expect(encode("(( @")).toBe("))((");
  });
  it("should encode 4 spaces", () => {
    expect(encode("    ")).toBe("))))");
  });
  it("should return empty string if empty input", () => {
    expect(encode("")).toBe("");
  });
});
