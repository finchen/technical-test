describe("Testing of environment", () => {

  it("should allow es2019 features", () => {
    expect(['Dog', ['Sheep', 'Wolf']].flat()).toStrictEqual(['Dog', 'Sheep', 'Wolf']);
  });

});
