import add, { addAnt, removeLeadingZeros } from "../src/add";

describe("add binary string", () => {

  it("should add 111 and 10", () => {
    expect(add('111', '10')).toBe('1001');
    expect(addAnt('111', '10')).toBe('1001');
  });
  it("should add 1101 and 101", () => {
    expect(add('1101', '101')).toBe('10010');
    expect(addAnt('1101', '101')).toBe('10010');
  });

  it("should add 1101 and 10111", () => {
    expect(add('1101', '10111')).toBe('100100');
    expect(addAnt('1101', '10111')).toBe('100100');
  });

  it("should add 10 and 11001100", () => {
    expect(add('10', '11001100')).toBe('11001110');
    expect(addAnt('10', '11001100')).toBe('11001110');
  });

  it("should add 00010 and 11001100", () => {
    expect(add('000010', '11001100')).toBe('11001110');
    expect(addAnt('00010', '11001100')).toBe('11001110');
  });

  it("should calculate with with remove the leading 0", () => {
    expect(add('000010', '0')).toBe('10');
    expect(addAnt('00010', '0')).toBe('10');
  });

  it("should return 0 when adding 0", () => {
    expect(add('0', '0')).toBe('0');
    expect(addAnt('0', '0')).toBe('0');
  });

  it("should return 0 when adding 00000", () => {
    expect(add('00000', '0')).toBe('0');
    expect(addAnt('00000', '0')).toBe('0');

    expect(add('00000', '00000')).toBe('0');
    expect(addAnt('00000', '00000')).toBe('0');
  });
});

describe("test removeLeadingZeros for binary result", () => {

  it("should remove leading zero", () => {
    expect(removeLeadingZeros('01')).toBe('1');
    expect(removeLeadingZeros('00001')).toBe('1');
  });

  it("should keep one zero if zero", () => {
    expect(removeLeadingZeros('0')).toBe('0');
    expect(removeLeadingZeros('0000')).toBe('0');
  });
});

