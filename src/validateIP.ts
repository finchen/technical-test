import { isIPv4 } from 'net';

/**
 * Write a function that will identify valid IPv4 addresses in dot-decimal format.
 */
const validateIP = (ip: string): boolean => {
  return isIPv4(ip);
}

/**
 * Write a function that will identify valid IPv4 addresses in dot-decimal format.
 * Note: with regexp and without js util function
 */
export const validateIP2 = (ip: string): boolean => {
  // regexp validate 4 groups of number separated with dot
  // valid number:
  // 200 to 255
  // 100 to 199
  // 10 to 99
  // 0-9
  const regex = /^(1[0-9][0-9]|2[0-5][0-5]|[1-9][0-9]|[0-9])\.(1[0-9][0-9]|2[0-5][0-5]|[1-9][0-9]|[0-9])\.(1[0-9][0-9]|2[0-5][0-5]|[1-9][0-9]|[0-9])\.(1[0-9][0-9]|2[0-5][0-5]|[1-9][0-9]|[0-9])$/;
  const matches = ip.match(regex);
  if (matches === null || matches.length !== 5) {
    return false;
  }
  return true;
}

export default validateIP;

